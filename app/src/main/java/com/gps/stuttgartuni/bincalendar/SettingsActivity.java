package com.gps.stuttgartuni.bincalendar;

import android.Manifest;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;

import org.joda.time.DateTime;

import static com.gps.stuttgartuni.bincalendar.Constants.CHOOSED_FLAT;
import static com.gps.stuttgartuni.bincalendar.Constants.FLAT_PROFILE;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_WEEK_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.endDate;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener ,
DatePickerFragment.DatePicked{

    private static final int PERMISSIONS_WRITE_CALENDAR = 405;
    private static final int PERMISSIONS_READ_CALENDAR = 406;
    private static final int CHANGE_CONFIGURATION_ACTIVITY_CODE = 32;


    private int roomNumber;
    private int startingWeek;
    private int startingRoom;
    private TrashRoutineManager trashRoutineManager;

    private SharedPreferenceManager sharedPreferences;
    private RemindersManager remindersManager;
    private SwitchPreference switchPreference;
    private Preference endDatePreference;
    private Preference editConfiguration;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roomNumber =getIntent().getIntExtra(ROOM_NUMBER, 0);
        startingWeek=getIntent().getIntExtra(STARTING_WEEK_NUMBER,0);
        startingRoom=getIntent().getIntExtra(STARTING_ROOM,0);
        sharedPreferences=new SharedPreferenceManager(this);
        addPreferencesFromResource(R.xml.preferences);
        switchPreference=(SwitchPreference)findPreference("cal_rem");
        endDatePreference=(Preference) findPreference("rem_end");
        editConfiguration=(Preference)findPreference(getString(R.string.change_configuration));
        checkThePreference();
        remindersManager=new RemindersManager(roomNumber,startingWeek,getApplicationContext());
        endDatePreference.setSummary(endDate.getDayOfMonth()+"/"+endDate.getMonthOfYear()+"/"+endDate.getYear());
        trashRoutineManager=new TrashRoutineManager(startingRoom);
        endDatePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showDatePickerDialog(findViewById(R.id.buttonStartConfiguration));
                remindersManager.deleteTrashEvents();

                return true;
            }
        });
        editConfiguration.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivityForResult(new Intent(getApplicationContext(),ChangeConfigurationActivity.class),CHANGE_CONFIGURATION_ACTIVITY_CODE);
                return true;
            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
    private void checkThePreference(){
        if(sharedPreferences.getBoolean(getString(R.string.SAVED_EVENTS_BOOLEAN),false)){
             switchPreference.setChecked(true);
            endDatePreference.setEnabled(true);

        }
    }


    private void checkPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    PERMISSIONS_WRITE_CALENDAR);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALENDAR},
                    PERMISSIONS_READ_CALENDAR);
        }
        else {
            showDatePickerDialog(findViewById(R.id.buttonStartConfiguration));
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSIONS_WRITE_CALENDAR:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    showDatePickerDialog(findViewById(R.id.buttonStartConfiguration));
                }
                else {
                    showCalendarRejectedDialog();
                    switchPreference.setChecked(false);
                }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CHANGE_CONFIGURATION_ACTIVITY_CODE && resultCode==RESULT_OK){
            sharedPreferences.fillSharedPreferences(data.getIntExtra(ROOM_NUMBER,0),data.getIntExtra(STARTING_ROOM,0),data.getStringExtra(CHOOSED_FLAT),data.getStringExtra(FLAT_PROFILE));
            trashRoutineManager.initHashMap(trashRoutineManager.fillRoomsArray(sharedPreferences.getString(FLAT_PROFILE,null)));
            if(switchPreference.isChecked()){
                showAlertCalendarChangeDialog();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("cal_rem")){
            if(switchPreference.isChecked()){
                checkPermission();

            }
            else {
               remindersManager.deleteTrashEvents();
                SharedPreferences.Editor editor=this.sharedPreferences.edit();
                editor.putBoolean(getString(R.string.SAVED_EVENTS_BOOLEAN),false);
                editor.apply();
                endDatePreference.setSummary(endDate.getDayOfMonth()+"/"+endDate.getMonthOfYear()+"/"+endDate.getYear());
                endDatePreference.setEnabled(false);

            }
        }
        if(key.equals("rem_end")){

        }
    }



    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(),"DATA_PICKER");

    }

    //METHOD DELETE ALL FROM CALENDAR!!!!
    private void cleanTheCalendar(){
        Uri EVENTS_URI = Uri.parse("content://com.android.calendar/" + "events");

        ContentResolver cr = getContentResolver();
        deleteEvent(cr, EVENTS_URI, 1);


    }
    private void deleteEvent(ContentResolver resolver, Uri eventsUri, int calendarId) {
        Cursor cursor;
        cursor = resolver.query(eventsUri, new String[]{ "_id" },  "calendar_id=" + calendarId, null, null);
        while(cursor.moveToNext()) {
            long eventId = cursor.getLong(cursor.getColumnIndex("_id"));
            resolver.delete(ContentUris.withAppendedId(eventsUri, eventId), null, null);
        }
        cursor.close();
    }
    @Override
    public void onDatePicked(int year, int month, int dayOfMonth) {
        endDate=new DateTime(year,month+1,dayOfMonth,8,20);
        trashRoutineManager.initHashMap(trashRoutineManager.fillRoomsArray(sharedPreferences.getString(FLAT_PROFILE,null)));
        remindersManager.addRemindersToCalendar();
        endDatePreference.setSummary(endDate.getDayOfMonth()+"/"+endDate.getMonthOfYear()+"/"+endDate.getYear());
        endDatePreference.setEnabled(true);
    }
    private void showCalendarRejectedDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.calendar_rejected_info)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAlertCalendarChangeDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Add to calendar")
                .setMessage("You have change your configuration. Should the application change you calendar reminders?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        remindersManager.deleteTrashEvents();
                        checkPermission();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
