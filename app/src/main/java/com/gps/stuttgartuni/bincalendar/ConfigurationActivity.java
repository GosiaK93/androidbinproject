package com.gps.stuttgartuni.bincalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gps.stuttgartuni.bincalendar.Constants.FLAT_PROFILE;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_LETTER;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;

public class ConfigurationActivity extends AppCompatActivity {

    @BindView(R.id.editTextRoomNumber)
    EditText mRoomNumber;
    @BindView(R.id.editTextStartingRoom)
    EditText mStartingRoom;
    @BindView(R.id.buttonSaveConfiguration)
    Button mSaveButton;
    @BindView(R.id.textViewWrongRoomNumber)
    TextView mWrongRoomNumber;
    @BindView(R.id.editTextHouseNumber)
    EditText mHouseNumer;
    @BindView(R.id.spinnerLetterChooser)
    Spinner mHouseLetter;

    private ConfigurationDataManager configurationDataManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);
        configurationDataManager=new ConfigurationDataManager();
        populateTheSpinner();
    }

    @OnClick(R.id.buttonSaveConfiguration)
    public void saveConfiguration(){
        Intent intent = new Intent();
        String profile=configurationDataManager.parseFlatProfile(mRoomNumber.getText().toString());
        if( profile!=null && configurationDataManager.parseFlatProfile(mStartingRoom.getText().toString())!=null) {
        intent.putExtra(ROOM_NUMBER,Integer.parseInt(mRoomNumber.getText().toString()));
        intent.putExtra(STARTING_ROOM,Integer.parseInt(mStartingRoom.getText().toString()));
            intent.putExtra(HOUSE_NUMBER,Integer.parseInt(mHouseNumer.getText().toString()));
            intent.putExtra(HOUSE_LETTER, mHouseLetter.getSelectedItem().toString());
            intent.putExtra(FLAT_PROFILE,profile);
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            mWrongRoomNumber.setVisibility(View.VISIBLE);
        }
    }



    private void populateTheSpinner(){
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.letters,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mHouseLetter.setAdapter(adapter);
    }
}
