package com.gps.stuttgartuni.bincalendar;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Gosia on 14.07.2017.
 */

public class HouseChooserListViewAdapter extends ArrayAdapter<String> {


    Context context;
    int resource;
    String [] data;


    public HouseChooserListViewAdapter(@NonNull Context context, @LayoutRes int resource, String[]data) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
        this.data=data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row=convertView;
        ViewHolder viewHolder=null;
        if(row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            viewHolder=new ViewHolder();
            viewHolder.mFlatName=(TextView)row.findViewById(R.id.textViewHouseName);
            viewHolder.mHouseImage=(ImageView)row.findViewById(R.id.imageViewHouse);

            row.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder)row.getTag();
        }
        String rowBean=data[position];
        viewHolder.mFlatName.setText(rowBean);
        return row;
    }

    static class ViewHolder{
        TextView mFlatName;
        ImageView mHouseImage;
    }


}
