package com.gps.stuttgartuni.bincalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import static com.gps.stuttgartuni.bincalendar.Constants.CONFIGURATION_REQUEST_CODE;

public class FirstStartActivity extends AppCompatActivity {

    Button mStartButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_start);

        mStartButton=(Button)findViewById(R.id.buttonStartConfiguration);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent configStartIntent=new Intent(getApplicationContext(),BuildingChooserActivity.class);
                startActivityForResult(configStartIntent,CONFIGURATION_REQUEST_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CONFIGURATION_REQUEST_CODE && resultCode==RESULT_OK){
            setResult(RESULT_OK,data);
            finish();
        }
    }

    @Override
    public void onBackPressed() {

    }
}
