package com.gps.stuttgartuni.bincalendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Gosia on 30.07.2017.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    DatePicked datePicked;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog=new DatePickerDialog(getActivity(), this, year, month, day);
        datePickerDialog.setTitle(getString(R.string.choose_the_date));
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               DatePicker datePicker= datePickerDialog.getDatePicker();
                onDateSet(datePickerDialog.getDatePicker(),datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
            }
        });
        // Create a new instance of DatePickerDialog and return it
        return datePickerDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            datePicked=(DatePicked)context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement DatePicked");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Context context=activity.getApplication().getApplicationContext();
        try{
            datePicked=(DatePicked)activity;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement DatePicked");
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if(!view.isShown()) return;
        datePicked.onDatePicked(year,month,dayOfMonth);
    }

    public interface DatePicked{
        public void onDatePicked(int year, int month, int dayOfMonth);
    }

}
