package com.gps.stuttgartuni.bincalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.gps.stuttgartuni.bincalendar.Constants.CHOOSED_FLAT;
import static com.gps.stuttgartuni.bincalendar.Constants.FLAT_PROFILE;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_LETTER;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.SELECTED_BUILDING;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;

public class ChangeConfigurationActivity extends AppCompatActivity {


    @BindView(R.id.spinnerHouseSelectorChange)
    Spinner mHouseSelector;
    @BindView(R.id.editTextRoomNumberChange)
    EditText mRoomNumber;
    @BindView(R.id.editTextStartingRoomChange)
    EditText mStartingRoom;
    @BindView(R.id.buttonSaveConfigurationChange)
    Button mSaveButton;
    @BindView(R.id.textViewWrongRoomNumberChange)
    TextView mWrongRoomNumber;
    @BindView(R.id.editTextHouseNumberChange)
    EditText mHouseNumer;
    @BindView(R.id.spinnerLetterChooserChange)
    Spinner mHouseLetter;

    private ConfigurationDataManager configurationDataManager;
    private SharedPreferenceManager sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_configuration);
        ButterKnife.bind(this);
        sharedPreferences=new SharedPreferenceManager(this);
        populateTheSpinner();
        configurationDataManager=new ConfigurationDataManager();
        mHouseNumer.setText(String.valueOf(sharedPreferences.getInt(HOUSE_NUMBER,0)));
        mRoomNumber.setText(String.valueOf(sharedPreferences.getInt(ROOM_NUMBER,0)));
        mStartingRoom.setText(String.valueOf(sharedPreferences.getInt(STARTING_ROOM,0)));
    }

    private void populateTheSpinner(){
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.letters,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mHouseLetter.setAdapter(adapter);
        int pos=adapter.getPosition(sharedPreferences.getString(HOUSE_LETTER,null));
        mHouseLetter.setSelection(pos);
        ArrayAdapter<CharSequence> adapterSelector=ArrayAdapter.createFromResource(this,R.array.Houses,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mHouseSelector.setAdapter(adapterSelector);
        String selectedBuilding=sharedPreferences.getString(SELECTED_BUILDING,null);
        int spinnerPosition = adapterSelector.getPosition(selectedBuilding);
        mHouseSelector.setSelection(spinnerPosition);

    }

    @OnClick(R.id.buttonSaveConfigurationChange)
    public void saveConfiguration(){
        Intent intent = new Intent();
        String profile=configurationDataManager.parseFlatProfile(mRoomNumber.getText().toString());
        if( profile!=null && configurationDataManager.parseFlatProfile(mStartingRoom.getText().toString())!=null) {
            intent.putExtra(ROOM_NUMBER,Integer.parseInt(mRoomNumber.getText().toString()));
            intent.putExtra(STARTING_ROOM,Integer.parseInt(mStartingRoom.getText().toString()));
            String selectedBuilding=mHouseSelector.getSelectedItem().toString();
            switch (selectedBuilding) {
                case "Pfaffenhof I" :
                    selectedBuilding="Pfaffenwaldring";
                    break;
                case "Pfaffenhof II" :
                    selectedBuilding="Pfaffenwaldring";
                    break;
                case "Allamandring I" :
                    selectedBuilding="Allamandring";
                    break;
                case "Allamandring II" :
                    selectedBuilding="Allamandring";
                    break;

            }
            int houseNumber= Integer.parseInt(mHouseNumer.getText().toString());
            String houseLetter=mHouseLetter.getSelectedItem().toString();
            intent.putExtra(CHOOSED_FLAT,selectedBuilding+" "+Integer.parseInt(mHouseNumer.getText().toString())+mHouseLetter.getSelectedItem().toString());
            intent.putExtra(FLAT_PROFILE,profile);
            sharedPreferences.saveExtras(houseNumber,houseLetter,selectedBuilding);
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            mWrongRoomNumber.setVisibility(View.VISIBLE);
        }
    }


}
