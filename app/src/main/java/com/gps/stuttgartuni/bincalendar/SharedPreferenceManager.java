package com.gps.stuttgartuni.bincalendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Map;
import java.util.Set;

import static com.gps.stuttgartuni.bincalendar.Constants.CHOOSED_FLAT;
import static com.gps.stuttgartuni.bincalendar.Constants.FLAT_PROFILE;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_LETTER;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.SELECTED_BUILDING;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_WEEK_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_YEAR;

/**
 * Created by Gosia on 04.08.2017.
 */

public class SharedPreferenceManager implements SharedPreferences {
SharedPreferences sharedPreferences;
Context mContex;

    public SharedPreferenceManager(Context mContex) {
        this.mContex = mContex;
        sharedPreferences=mContex.getSharedPreferences("fixedDateSharedPreferences", Context.MODE_PRIVATE);
    }

    public void fillSharedPreferences(int roomNumber, int startingRoom, String selectedBuilding, String selectedProfile){
        SharedPreferences.Editor edit=sharedPreferences.edit();
        edit.putInt(STARTING_ROOM,startingRoom);
        edit.putInt(ROOM_NUMBER,roomNumber);
        edit.putInt(STARTING_WEEK_NUMBER, Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
        edit.putInt(STARTING_YEAR,Calendar.getInstance().get(Calendar.YEAR));
        edit.putString(CHOOSED_FLAT,selectedBuilding);
        edit.putString(FLAT_PROFILE,selectedProfile);
        edit.apply();

    }
    public void saveExtras(int houseNumber, String houseLetter, String selectedBuilding){
        SharedPreferences.Editor edit=sharedPreferences.edit();
        edit.putInt(HOUSE_NUMBER,houseNumber);
        edit.putString(HOUSE_LETTER,houseLetter);
        edit.putString(SELECTED_BUILDING,selectedBuilding);
        edit.apply();
    }


    @Override
    public Map<String, ?> getAll() {
        return null;
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return sharedPreferences.getString(key,defValue);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, @Nullable Set<String> defValues) {
        return sharedPreferences.getStringSet(key,defValues);
    }

    @Override
    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key,defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return getLong(key,defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return sharedPreferences.getFloat(key,defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key,defValue);
    }

    @Override
    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    @Override
    public Editor edit() {
        return sharedPreferences.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }
}
