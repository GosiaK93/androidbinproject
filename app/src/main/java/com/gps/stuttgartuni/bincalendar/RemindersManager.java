package com.gps.stuttgartuni.bincalendar;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.CalendarContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_YEAR;
import static com.gps.stuttgartuni.bincalendar.Constants.calculateNumbersOfWeeks;
import static com.gps.stuttgartuni.bincalendar.Constants.kindsArray;
import static com.gps.stuttgartuni.bincalendar.Constants.weekList;

/**
 * Created by Gosia on 28.07.2017.
 */

public class RemindersManager {
    private SharedPreferences sharedPreferences;
    private int roomNumber;
    private int startingWeek;

    private static final String SAVED_EVENTS = "SAVED_EVENTS";
    private Set<String> evensIDs;
    private Context context;
    private int startingYear;

    public RemindersManager(int roomNumber, int startingWeek, Context context) {
        this.roomNumber = roomNumber;
        this.startingWeek = startingWeek;
        this.context = context;
        this.sharedPreferences=context.getSharedPreferences("fixedDateSharedPreferences", Context.MODE_PRIVATE);
        evensIDs=new HashSet<>();
        this.startingYear=sharedPreferences.getInt(STARTING_YEAR,0);
    }

    public void addRemindersToCalendar() {
        HashMap<String, ArrayList<Integer>> calendarHashMap = new HashMap<>();
        Calendar cal=Calendar.getInstance();
        ArrayList<WeekHolder> ret=new ArrayList<>();
        for(int i=convertWeekToListIndex(); i<weekList.size(); i++) {
            HashMap<String,Integer> searchHashMap=weekList.get(i);
            for (String kind : kindsArray) {
                if (searchHashMap.get(kind) == roomNumber) {
                    ret.add(new WeekHolder(kind,cal.get(Calendar.WEEK_OF_YEAR)+i));
                    break;
                }
            }
        }

        for(WeekHolder wk:ret) {
            addCalendarEvent(Calendar.FRIDAY,wk.getWeek(),"Pick up the trash "+"("+wk.getKind()+")");
            addCalendarEvent(Calendar.MONDAY,wk.getWeek(),"This week is your turn to pick up the trash "+"("+wk.getKind()+")");
            addCalendarEvent(Calendar.SUNDAY,wk.getWeek(),"Did you pick up the trash?"+"("+wk.getKind()+")");
        }
        saveInSharedPreferences();
    }


    private void addCalendarEvent(int eventDay,int week, String eventTitle){
        long calID=1;
        long startMillis = 0;
        long endMillis = 0;

        TimeZone timeZone = TimeZone.getDefault();
        Calendar beginTime = Calendar.getInstance(timeZone);
        ContentResolver cr = context.getContentResolver();

        beginTime.set(Calendar.HOUR_OF_DAY,7);
        beginTime.set(Calendar.MINUTE,0);
        beginTime.set(Calendar.DAY_OF_WEEK,eventDay);
        beginTime.set(Calendar.WEEK_OF_YEAR,week);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance(timeZone);
        endTime.set(Calendar.HOUR_OF_DAY,8);
        endTime.set(Calendar.MINUTE,0);
        endTime.set(Calendar.DAY_OF_WEEK, eventDay);
        endTime.set(Calendar.WEEK_OF_YEAR, week);
        endMillis = endTime.getTimeInMillis();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, eventTitle);
        values.put(CalendarContract.Events.DESCRIPTION, eventTitle);
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "UTC");
        values.put(CalendarContract.Events.HAS_ALARM,1);

        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        long eventID = Long.parseLong(uri.getLastPathSegment());
        evensIDs.add(String.valueOf(eventID));
        ContentValues valuesReminder = new ContentValues();
        valuesReminder.put(CalendarContract.Reminders.MINUTES, 15);
        valuesReminder.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        valuesReminder.put(CalendarContract.Reminders.EVENT_ID, eventID);
        Uri uri2 = cr.insert(CalendarContract.Reminders.CONTENT_URI, valuesReminder);
    }

    public Set<String> getEvensIDs() {
        return evensIDs;
    }

    private void saveInSharedPreferences(){
        SharedPreferences.Editor edit=sharedPreferences.edit();
        Set<String> set = new HashSet<>();
        set.addAll(evensIDs);
        edit.putStringSet(SAVED_EVENTS,set);
        edit.apply();
    }


    public void deleteTrashEvents(){
        evensIDs=new HashSet<>();
        evensIDs=sharedPreferences.getStringSet(SAVED_EVENTS,new HashSet<String>());
        for(String l:evensIDs){
            deleteCalendarEntry(Long.parseLong(l));
        }
    }
    private int deleteCalendarEntry(Long entryID) {
        int iNumRowsDeleted = 0;
        Uri eventsUri =CalendarContract.Events.CONTENT_URI;
        Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);
        iNumRowsDeleted = context.getContentResolver().delete(eventUri, null, null);
        return iNumRowsDeleted;
    }
    private class WeekHolder {
        private String kind;
        private int week;

        public WeekHolder(String kind, int week) {
            this.kind = kind;
            this.week = week;
        }

        public String getKind() {
            return kind;
        }

        public int getWeek() {
            return week;
        }
    }
    private int convertWeekToListIndex(){
        Calendar calendar=Calendar.getInstance();
        return calendar.get(Calendar.WEEK_OF_YEAR)+(calendar.get(Calendar.YEAR)-startingYear)*calculateNumbersOfWeeks(calendar.get(Calendar.YEAR)-1)-startingWeek;
    }

}

