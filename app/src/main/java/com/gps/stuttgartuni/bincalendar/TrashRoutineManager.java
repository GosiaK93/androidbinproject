package com.gps.stuttgartuni.bincalendar;

import org.joda.time.DateTime;
import org.joda.time.Weeks;

import java.util.Calendar;
import java.util.HashMap;

import static com.gps.stuttgartuni.bincalendar.Constants.endDate;
import static com.gps.stuttgartuni.bincalendar.Constants.kindsArray;
import static com.gps.stuttgartuni.bincalendar.Constants.pfaffenhofLower;
import static com.gps.stuttgartuni.bincalendar.Constants.pfaffenhofUpper;
import static com.gps.stuttgartuni.bincalendar.Constants.weekList;

/**
 * Created by Gosia on 04.08.2017.
 */

public class TrashRoutineManager {

    private int startingRoom;

    public TrashRoutineManager(int startingRoom) {
        this.startingRoom = startingRoom;
    }

    public void initHashMap(Integer[] rooms){
        //Start filling the array from current year
        weekList.clear();
        int roomIterator=0;
        for (int i=0; i<rooms.length; i++){
            if (rooms[i]==startingRoom){
                roomIterator=i;
                break;
            }
        }
        int numberOfWeeks=getWeekDifference(Calendar.getInstance().getTime().getTime(),endDate.getMillis());
        for (int i = 0; i<=numberOfWeeks+1; i++){
            HashMap<String,Integer> hashMap=new HashMap<>();
            for (String kind : kindsArray) {
                hashMap.put(kind,rooms[roomIterator]);
                roomIterator++;
                if (roomIterator>=rooms.length){
                    roomIterator=0;
                }
            }
            weekList.add(hashMap);
        }
    }
    public Integer[] fillRoomsArray(String profile){
        switch (profile){
            case "Pfaffenhof upper":
                return  pfaffenhofUpper;
            case "Pfaffenhof lower":
                return pfaffenhofLower;
        }
        return null;
    }

    private int calculateNumbersOfWeeks(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);

        int ordinalDay = cal.get(Calendar.DAY_OF_YEAR);
        int weekDay = cal.get(Calendar.DAY_OF_WEEK) - 1; // Sunday = 0
        int numberOfWeeks = (ordinalDay - weekDay + 10) / 7;
        return numberOfWeeks;
    }

    private int getWeekDifference(long fromDateInMills, long toDateInMills) {
        int weeks = Weeks.weeksBetween(new DateTime(fromDateInMills), new DateTime(toDateInMills)).getWeeks();
        if (weeks < 0) {
            weeks = Weeks.weeksBetween(new DateTime(toDateInMills), new DateTime(fromDateInMills)).getWeeks();
        }
        return weeks;
    }

}
