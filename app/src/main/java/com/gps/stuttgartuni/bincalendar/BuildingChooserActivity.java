package com.gps.stuttgartuni.bincalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

import static com.gps.stuttgartuni.bincalendar.Constants.CHOOSED_FLAT;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_LETTER;
import static com.gps.stuttgartuni.bincalendar.Constants.HOUSE_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.SELECT_HOUSE_NUMBER_ACTIVITY_CODE;

public class BuildingChooserActivity extends AppCompatActivity {

    ListView mHouseChooser;
    String selectedBuilding;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_chooser);
        ButterKnife.bind(this);
        mHouseChooser=(ListView)findViewById(R.id.listViewHouseChooser);

        final HouseChooserListViewAdapter houseChooserListViewAdapter =new HouseChooserListViewAdapter(this,R.layout.flat_cell,getResources().getStringArray(R.array.Houses));
        mHouseChooser.setAdapter(houseChooserListViewAdapter);
        mHouseChooser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedBuilding= houseChooserListViewAdapter.data[position];
                Intent selectProfileIntent=new Intent(getApplicationContext(),ConfigurationActivity.class);
                startActivityForResult(selectProfileIntent,SELECT_HOUSE_NUMBER_ACTIVITY_CODE);
            }
        });


/*        expListView = (ExpandableListView) findViewById(R.id.expandableListViewHouseChooser);
        getDataLists();
        listAdapter=new HousePickerExpandableAdapter(this,listDataHeader,listDataChild);
        expListView.setAdapter(listAdapter);*/


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        SharedPreferenceManager sharedPreferenceManager=new SharedPreferenceManager(this);
        sharedPreferenceManager.saveExtras(data.getIntExtra(HOUSE_NUMBER,0),data.getStringExtra(HOUSE_LETTER),selectedBuilding);
        if(requestCode==SELECT_HOUSE_NUMBER_ACTIVITY_CODE && resultCode==RESULT_OK){
            switch (selectedBuilding) {
                case "Pfaffenhof I" :
                    selectedBuilding="Pfaffenwaldring";
                 break;
                case "Pfaffenhof II" :
                    selectedBuilding="Pfaffenwaldring";
                    break;
                case "Allamandring I" :
                    selectedBuilding="Allamandring";
                    break;
                case "Allamandring II" :
                    selectedBuilding="Allamandring";
                    break;

            }
            data.putExtra(CHOOSED_FLAT, selectedBuilding+" "+data.getIntExtra(HOUSE_NUMBER,0)+" "+data.getStringExtra(HOUSE_LETTER));
            setResult(RESULT_OK,data);
            finish();
        }

    }
    private void getDataLists(){
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        Collections.addAll(listDataHeader, getResources().getStringArray(R.array.Houses));
        for (String s: listDataHeader){
            listDataChild.put(s, Arrays.asList(getResources().getStringArray(R.array.FlatsPfaffenhof)));
        }


    }
}
