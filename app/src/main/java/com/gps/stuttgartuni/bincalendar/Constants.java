package com.gps.stuttgartuni.bincalendar;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Gosia on 14.07.2017.
 */

public final class Constants {
    public static final int NOTIFICATION_IDENTIFIER = 1;
    public static final String STARTING_ROOM = "Starting room";
    public static final String STARTING_WEEK_NUMBER ="STARTING_WEEK_NUMBER" ;
    public static final String TRASH_FULL ="TRASH_FULL";
    public static final String FLAT_PROFILE ="FLAT_PROFILE";
    public static final String ROOM_NUMBER ="ROOM_NUMBER";
    public static final String CHOOSED_FLAT = "CHOOSED_FLAT";
    public static final String CURRENT_WEEK_ROOMS = "CURRENT_WEEK_ROOMS";
    public static final String NEXT_TURN ="NEXT_TURN";
    public static final String NEXT_TURN_END = "NEXT_TURN_END";
    public static final String HOUSE_NUMBER ="HOUSE_NUMBER";
    public static final String HOUSE_LETTER = "HOUSE_LETTER";
    public static final String SAVED_EVENTS = "SAVED_EVENTS";
    public static final String STARTING_YEAR = "STARTING_YEAR";
    public static final String SELECTED_BUILDING ="SELECTED_BUILDING" ;

    public static final int CONFIGURATION_RESULT_CODE = 10;
    public static final int CONFIGURATION_REQUEST_CODE = 15;
    public static final int SELECT_ROOM_INTENT_CODE = 10;
    public static final int SELECT_PROFILE_ACTIVITY_CODE = 22;
    public static final int SELECT_HOUSE_NUMBER_ACTIVITY_CODE = 20;

    public static final int PERMISSIONS_WRITE_CALENDAR = 405;
    public static final int PERMISSIONS_READ_CALENDAR = 406;


    public static final String kindsArray[]={"organic","plastic","paper","glass"};
    public static final Integer []pfaffenhofUpper={402,403,404,405,406,501,502,503,504,505,506};
    public static final Integer []pfaffenhofLower={202,203,204,205,206,301,302,303,304,305,306};

    public static DateTime endDate;
    public static ArrayList<HashMap<String,Integer>> weekList=new ArrayList<>();
    public static int calculateNumbersOfWeeks(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);

        int ordinalDay = cal.get(Calendar.DAY_OF_YEAR);
        int weekDay = cal.get(Calendar.DAY_OF_WEEK) - 1; // Sunday = 0
        int numberOfWeeks = (ordinalDay - weekDay + 10) / 7;
        return numberOfWeeks;
    }

}
