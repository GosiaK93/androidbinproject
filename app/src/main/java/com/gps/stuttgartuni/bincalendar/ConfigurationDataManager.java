package com.gps.stuttgartuni.bincalendar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Gosia on 04.08.2017.
 */

public class ConfigurationDataManager {


    public String parseFlatProfile(String roomNumber){
        Pattern upperPattern=Pattern.compile("40[2-6]|50[2-6]");
        Pattern lowerPattern=Pattern.compile("20[2-6]|30[2-6]");
        Matcher upperMatcher=upperPattern.matcher(roomNumber);
        if(upperMatcher.matches()){
            return "Pfaffenhof upper";
        }
        if (lowerPattern.matcher(roomNumber).matches()){
            return "Pfaffenhof down";
        }
        return null;
    }


}

