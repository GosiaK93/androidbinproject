package com.gps.stuttgartuni.bincalendar;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static com.gps.stuttgartuni.bincalendar.Constants.CURRENT_WEEK_ROOMS;
import static com.gps.stuttgartuni.bincalendar.Constants.NEXT_TURN;
import static com.gps.stuttgartuni.bincalendar.Constants.NEXT_TURN_END;
import static com.gps.stuttgartuni.bincalendar.Constants.NOTIFICATION_IDENTIFIER;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_WEEK_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_YEAR;
import static com.gps.stuttgartuni.bincalendar.Constants.TRASH_FULL;
import static com.gps.stuttgartuni.bincalendar.Constants.calculateNumbersOfWeeks;
import static com.gps.stuttgartuni.bincalendar.Constants.kindsArray;
import static com.gps.stuttgartuni.bincalendar.Constants.weekList;

/**
 * Created by Gosia on 03.07.2017.
 */

public class Service extends android.app.Service {


    private Handler mHandler=new Handler();
    private Constants constants=new Constants();
    private int startingRoom;
    private int startingWeek;
    private int roomNumber;
    private int startingYear;


    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }
    @Override
    public void onCreate() {
         mHandler = new Handler();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startingRoom=intent.getIntExtra(STARTING_ROOM,0);
        startingWeek=intent.getIntExtra(STARTING_WEEK_NUMBER,0);
        startingYear=intent.getIntExtra(STARTING_YEAR,0);
        roomNumber=intent.getIntExtra(ROOM_NUMBER,0);
        String kindOfRubbish =checkTheWeek(roomNumber);
        Intent broadcastIntent=new Intent("Service Message");
        if (kindOfRubbish!=null){
            //showNotification(kindOfRubbish);
            broadcastIntent.putExtra(TRASH_FULL,true);
        }
        else {
            //cancelNotification();
            broadcastIntent.putExtra(TRASH_FULL,false);
        }

        HashMap<String,Integer> currentWeek=weekList.get(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)-startingWeek);
        broadcastIntent.putExtra(CURRENT_WEEK_ROOMS,currentWeek);
        broadcastIntent.putExtra(NEXT_TURN,getDate(Calendar.MONDAY,false));
        broadcastIntent.putExtra(NEXT_TURN_END,getDate(Calendar.MONDAY,true));
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {

    }

    private Integer findMyRoom(){
        Calendar cal=Calendar.getInstance();
        int week=convertWeekToListIndex();
        int ret=0;
        for(int i=week; i<weekList.size(); i++) {
            HashMap<String,Integer> searchHashMap=weekList.get(i);
            for (String kind : kindsArray) {
                if (searchHashMap.get(kind) == roomNumber) {
                     ret=cal.get(Calendar.WEEK_OF_YEAR)+i;
                    return ret;
                }
            }
        }
        return ret;
    }
    private String getDate(int calendarDay, boolean end){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        int week=findMyRoom();
        if(end){
            week++;
        }
        String sp=sdf.format(cal.getTime());

        cal.set(Calendar.DAY_OF_WEEK, calendarDay);
        cal.set(Calendar.WEEK_OF_YEAR, week);
        String s=sdf.format(cal.getTime());
        return s;

    }

    private void showNotification(String kind){
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle("Pick up trash!")
                        .setOngoing(true)
                        .setAutoCancel(false)
                        .setContentText("The trash is waiting for you :) ("+kind+")");

        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_IDENTIFIER, mBuilder.build());

    }

    private void cancelNotification(){
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_IDENTIFIER);

    }

    private String checkTheWeek(int roomNumber){
        HashMap<String,Integer> currentWeek=weekList.get(convertWeekToListIndex());
        for (String kind:kindsArray){
            if(currentWeek.get(kind)==roomNumber){
                return kind;}
        }
        return null;
    }
    private int convertWeekToListIndex(){
        Calendar calendar=Calendar.getInstance();
        return calendar.get(Calendar.WEEK_OF_YEAR)+(calendar.get(Calendar.YEAR)-startingYear)*calculateNumbersOfWeeks(calendar.get(Calendar.YEAR)-1)-startingWeek;
    }

}
