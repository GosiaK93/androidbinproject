package com.gps.stuttgartuni.bincalendar;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.gps.stuttgartuni.bincalendar.Constants.CHOOSED_FLAT;
import static com.gps.stuttgartuni.bincalendar.Constants.CONFIGURATION_RESULT_CODE;
import static com.gps.stuttgartuni.bincalendar.Constants.CURRENT_WEEK_ROOMS;
import static com.gps.stuttgartuni.bincalendar.Constants.FLAT_PROFILE;
import static com.gps.stuttgartuni.bincalendar.Constants.NEXT_TURN;
import static com.gps.stuttgartuni.bincalendar.Constants.NEXT_TURN_END;
import static com.gps.stuttgartuni.bincalendar.Constants.PERMISSIONS_READ_CALENDAR;
import static com.gps.stuttgartuni.bincalendar.Constants.PERMISSIONS_WRITE_CALENDAR;
import static com.gps.stuttgartuni.bincalendar.Constants.ROOM_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_ROOM;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_WEEK_NUMBER;
import static com.gps.stuttgartuni.bincalendar.Constants.STARTING_YEAR;
import static com.gps.stuttgartuni.bincalendar.Constants.TRASH_FULL;
import static com.gps.stuttgartuni.bincalendar.Constants.endDate;

public class MainActivity extends AppCompatActivity implements DatePickerFragment.DatePicked{


    @BindView(R.id.textViewdRoomNumber)
    TextView mRoomNumber;
    @BindView(R.id.textViewTurn)
    TextView mTurnIndicator;
    @BindView(R.id.textViewOrgranicRoom)
    TextView mOrganicRoom;
    @BindView(R.id.textViewPlasticRoom)
    TextView mPlasticRoom;
    @BindView(R.id.textViewPaperRoom)
    TextView mPaperRoom;
    @BindView(R.id.textViewGlassRoom)
    TextView mGlassRoom;
    @BindView(R.id.textViewHouse)
    TextView mHouse;
    @BindView(R.id.textViewKitchen)
    TextView mKitchen;



    @BindView(R.id.imageViewTrash)
    ImageView mTrashView;


    private SharedPreferenceManager sharedPreferences;
    private Intent serviceStartIntend;

    private int roomNumber;
    private int startingRoom;
    private TrashRoutineManager trashRoutineManager;

    private RemindersManager remindersManager;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Calendar cal=Calendar.getInstance();
        int endDay=31;
        int endMonth=12;
        int endYear=Calendar.getInstance().get(Calendar.YEAR);

        endDate= new DateTime(endYear,endMonth,endDay,8,20);

        sharedPreferences=new SharedPreferenceManager(this);

    }

    private void startService(){
       serviceStartIntend=new Intent(this, Service.class);
        serviceStartIntend.putExtra(STARTING_ROOM,sharedPreferences.getInt(STARTING_ROOM,0));
        serviceStartIntend.putExtra(ROOM_NUMBER,sharedPreferences.getInt(ROOM_NUMBER,0));
        serviceStartIntend.putExtra(FLAT_PROFILE,sharedPreferences.getString(FLAT_PROFILE,null));
        serviceStartIntend.putExtra(STARTING_WEEK_NUMBER,sharedPreferences.getInt(STARTING_WEEK_NUMBER,0));
        serviceStartIntend.putExtra(STARTING_YEAR,sharedPreferences.getInt(STARTING_YEAR,0));
        if(isMyServiceRunning(Service.class)){
            stopService(serviceStartIntend);
        }
        setViews();
        startService(serviceStartIntend);



    }

    private void setViews(){
        mRoomNumber.setText(String.valueOf(sharedPreferences.getInt(ROOM_NUMBER, 0)));
        mHouse.setText(String.valueOf(sharedPreferences.getString(CHOOSED_FLAT,"--")));
        mKitchen.setText(String.valueOf(sharedPreferences.getString(FLAT_PROFILE,"--")));
    }

    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(br, new IntentFilter("Service Message"));
        if(sharedPreferences.getString(CHOOSED_FLAT,null)==null){
            Intent configurationStartIntend=new Intent(this,FirstStartActivity.class);
            startActivityForResult(configurationStartIntend,CONFIGURATION_RESULT_CODE);

        }
        else {
            startingRoom=sharedPreferences.getInt(STARTING_ROOM,0);
            trashRoutineManager=new TrashRoutineManager(startingRoom);
            trashRoutineManager.initHashMap(trashRoutineManager.fillRoomsArray(sharedPreferences.getString(FLAT_PROFILE,"")));
            startService();
        }
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuItemConfigureApp:
               Intent intent=new Intent(getApplicationContext(),SettingsActivity.class);
                intent.putExtra(ROOM_NUMBER,sharedPreferences.getInt(ROOM_NUMBER,0));
                intent.putExtra(STARTING_WEEK_NUMBER,sharedPreferences.getInt(STARTING_WEEK_NUMBER,0));
                intent.putExtra(STARTING_ROOM,sharedPreferences.getInt(STARTING_ROOM,0));
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver br=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getBooleanExtra(TRASH_FULL,true)) {
                mTurnIndicator.setText(R.string.your_turn);
                mTrashView.setImageResource(R.drawable.trash);
            }
            else {
                mTurnIndicator.setText(R.string.free_this_week);
                mTrashView.setImageResource(R.drawable.donothing);
            }
            mTurnIndicator.setText(intent.getStringExtra(NEXT_TURN)+" - "+intent.getStringExtra(NEXT_TURN_END));
            HashMap<String,Integer> currentWeek=(HashMap<String, Integer>) intent.getSerializableExtra(CURRENT_WEEK_ROOMS);
            mOrganicRoom.setText(String.valueOf(currentWeek.get("organic")));
            mPlasticRoom.setText(String.valueOf(currentWeek.get("plastic")));
            mPaperRoom.setText(String.valueOf(currentWeek.get("paper")));
            mGlassRoom.setText(String.valueOf(currentWeek.get("glass")));
            trashRoutineManager=new TrashRoutineManager(sharedPreferences.getInt(STARTING_ROOM,0));
            stopService(serviceStartIntend);
        }
    };


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.option_menu,menu);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CONFIGURATION_RESULT_CODE && resultCode==RESULT_OK){
            roomNumber=data.getIntExtra(ROOM_NUMBER,0);
            startingRoom=data.getIntExtra(STARTING_ROOM,0);
            String selectedBuilding=data.getStringExtra(CHOOSED_FLAT);
            String selectedProfile=data.getStringExtra(FLAT_PROFILE);

            sharedPreferences.fillSharedPreferences(roomNumber,startingRoom,selectedBuilding,selectedProfile);
            trashRoutineManager=new TrashRoutineManager(startingRoom);
            trashRoutineManager.initHashMap(trashRoutineManager.fillRoomsArray(sharedPreferences.getString(FLAT_PROFILE,null)));
            startService();
            showAlertCalendarDialog();

        }

    }


    private void showAlertCalendarDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Add to calendar")
                .setMessage("In order to make app working more reliably it is recommended to add reminder to your calendar" +
                        "You can always change this option in the aplication menu"
                +"Do you want to continue?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        checkPermission();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void checkPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_CALENDAR},
                    PERMISSIONS_WRITE_CALENDAR);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALENDAR},
                    PERMISSIONS_READ_CALENDAR);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISSIONS_WRITE_CALENDAR:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    showDatePickerDialog(findViewById(R.id.buttonStartConfiguration));
                }
                else {
                    showCalendarRejectedDialog();

                }
        }
    }


    @Override
    public void onDatePicked(int year, int month, int dayOfMonth) {
        endDate= new DateTime(year,month+1,dayOfMonth,8,20);
        trashRoutineManager.initHashMap(trashRoutineManager.fillRoomsArray(sharedPreferences.getString(FLAT_PROFILE,null)));
        remindersManager=new RemindersManager(sharedPreferences.getInt(ROOM_NUMBER,0),sharedPreferences.getInt(STARTING_WEEK_NUMBER,0),getApplicationContext());
        remindersManager.addRemindersToCalendar();
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(getString(R.string.SAVED_EVENTS_BOOLEAN),true);
        editor.apply();
    }

    public void showDatePickerDialog(View v) {
        final DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(),"DATA_PICKER");

    }

    private void showCalendarRejectedDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.calendar_rejected_info)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}


